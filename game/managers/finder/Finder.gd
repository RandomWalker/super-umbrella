var stage = null
var stage_controller = null

#Finder will only work on stages under Stage_Controller
func _init(tree):
	if tree.get_root().has_node("Stage_Controller"):
		stage_controller = tree.get_root().get_node("Stage_Controller")
		stage = tree.get_root().get_node("Stage_Controller/Stage")
	else:
		stage = tree.get_root().get_node("Stage")

#Basic Nodes
func has_stage_controller():
	return stage_controller != null

func get_stage_controller():
	return stage_controller

func get_manager(manager):
	return stage.get_node("Managers/"+manager)

func has_manager(manager):
	return stage.has_node("Managers/"+manager)


func get_entity(entity):
	return stage.get_node("Entities/"+entity)

func has_entity(entity):
	return stage.has_node("Entities/"+entity)


func get_HUD(hud):
	return stage.get_node("HUD/"+hud)

func has_HUD(hud):
	return stage.has_node("HUD/"+hud)


#Specific Nodes

#Pause_Manager AKA "Pauser" ( the one that pauses )
func get_pauser():
	return stage.get_node("Managers/Pause_Manager")

func has_pauser():
	return stage.has_node("Managers/Pause_Manager")

#StageTransitions AKA "Transitor"
func get_transitor():
	return stage.get_node("HUD/StageTransition")

func has_transitor():
	return stage.get_node("HUD/StageTransition")	

#Save_Manager AKA "Saver"
func get_saver():
	return stage.get_node("Managers/Save_Manager")

func has_saver():
	return stage.has_node("Managers/Save_Manager")