extends Node

var multiplier = 2
var duration = 5

func new(mult, dur):
	multiplier = mult
	duration = dur

func _ready():
	if get_parent().has_node("Jumper"):
		get_parent().get_node("Jumper").jump_buff = multiplier
	$Timer.wait_time = duration
	$Timer.start()

func _on_Timer_timeout():
	if get_parent().has_node("Jumper"):
		get_parent().get_node("Jumper").jump_buff = 1
	queue_free()
